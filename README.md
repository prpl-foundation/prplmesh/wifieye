# wifiEye

wifiEye allows to read CSI data from unix socket opened by whm vendor modules and display those data in the console output.
The output format is the same format used by hexdump.

## Build

```
Make App :

git clone https://gitlab.com/prpl-foundation/prplmesh/wifieye.git wifieye
cd wifieye
make all

Clean App :

make clean

```

## Run

```
Usage :

wifiEye <csi unix socket path>

Example of output format against Maxlinear whm vendor module :

# wifiEye /var/run/whm-csi.sock

00000310  00 00 9A C7 12 1B 9B 41 00 00 00 00 05 01 6C FF  |.......A......l.|
00000320  91 FF 8C FF 63 FF 42 FF 26 16 00 00 01 00 28 00  |....c.B.&.....(.|
00000330  14 00 30 00 00 00 01 96 FF 00 00 00 00 00 00 00  |..0.............|
00000340  00 00 02 00 00 0A 30 00 00 0B 31 03 00 0C 07 0D  |......0...1.....|
00000350  D8 0D 75 4F FD 00 A5 80 F3 01 01 3F 0B 02 00 00  |..uO.......?....|
00000360  00 03 00 00 00 04 60 BF FC 00 C7 D0 F1 01 FE 2E  |......`.........|
00000370  0C 02 00 00 00 03 00 00 00 04 3A FF FC 00 EE 30  |..........:....0|
00000380  F0 01 07 2F 0D 02 00 00 00 03 00 00 00 04 20 BF  |.../.......... .|
00000390  FC 00 21 81 EE 01 15 CF 0E 02 00 00 00 03 00 00  |..!.............|
000003A0  00 04 03 0F FC 00 5D 31 ED 01 35 CF 10 02 00 00  |......]1..5.....|

```
