LDLIBS := $(if $(STAGINGDIR), -L$(STAGINGDIR)/lib) $(if $(STAGINGDIR), -L$(STAGINGDIR)/usr/lib)
CFLAGS := -Wall -I./include -I$(STAGINGDIR)/include/ -I$(STAGINGDIR)/usr/include/

SRC_DIR := src
APP_DIR := $(SRC_DIR)/App

APP_SRCS := $(wildcard $(APP_DIR)/*.c)
APP_OBJS := $(patsubst $(APP_DIR)/%.c, $(APP_DIR)/%.o, $(APP_SRCS))

OUTPUT := wifiEye

all: $(OUTPUT)

$(APP_DIR)/%.o: $(APP_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(OUTPUT): $(APP_OBJS)
	$(CC) $(CFLAGS) $^ -o $@  $(LDFLAGS) $(LDLIBS)

clean:
	rm -f $(APP_OBJS) $(OUTPUT)

.PHONY: all clean



