/* This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/socket.h>
#include <sys/un.h>

bool running = true;

void sig_handler(int signum) {
    if((signum == SIGINT) || (signum == SIGTERM)) {
        running = false;
    }
}

static void s_printHexDump(char* data, size_t dataSize) {
    size_t i, j;

    for(i = 0; i < dataSize; i += 16) {
        printf("%08lX  ", i);

        for(j = 0; j < 16; j++) {
            if(i + j < dataSize) {
                printf("%02X ", (unsigned char) data[i + j]);
            } else {
                printf("   "); // Padding for the last line if needed
            }
        }

        printf(" |");

        for(j = 0; j < 16; j++) {
            if(i + j < dataSize) {
                if(isprint((unsigned char) data[i + j])) {
                    printf("%c", (unsigned char) data[i + j]);
                } else {
                    printf(".");
                }
            }
        }

        printf("|\n");
    }
}

int main(int argc, char* argv[]) {

    signal(SIGINT, sig_handler);
    signal(SIGTERM, sig_handler);

    if(argc < 2) {
        printf("Usage: %s <csi unix socket path> \n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int sockfd;
    struct sockaddr_un serverAddr;

    // Create a new socket
    sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(sockfd == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // Set up the server address
    memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.sun_family = AF_UNIX;
    strncpy(serverAddr.sun_path, argv[1], sizeof(serverAddr.sun_path) - 1);

    // Connect to server
    if(connect(sockfd, (struct sockaddr*) &serverAddr, sizeof(serverAddr)) < 0) {
        perror("connect");
        close(sockfd);
        exit(EXIT_FAILURE);
    }

    // Receive data from server
    int bufferSize = 1024;
    char buffer[bufferSize];

    printf("%s starting !\n", argv[0]);
    while(running) {
        int recvBytes = recv(sockfd, buffer, bufferSize - 1, 0);
        if(recvBytes <= 0) {
            if(recvBytes == 0) {
                printf("server disconnected \n");
            } else {
                perror("recv");
            }
            break;
        } else {
            buffer[recvBytes] = '\0';
            s_printHexDump(buffer, recvBytes);
        }
    }

    // Close the socket when done
    printf("%s exiting !\n", argv[0]);
    close(sockfd);
    return EXIT_SUCCESS;
}
